module HtmlTable
  class Element
    attr_accessor :children
    attr_accessor :attributes
    attr_reader :tag

    def initialize(tag)
      @tag = tag
      @children = []
      @attributes = {}
      @text = nil
    end

    def to_html
      "<#{tag}#{inline_attributes}>#{children_html}#{@text}</#{tag}>"
    end

    def children_html
      children.map(&:to_html).join('')
    end

    def text(value)
      @text = value
    end

    def classes(value)
      attributes[:class] = value
    end

    def find_child_and_eval(human_index, &block)
      child = children[human_index - 1]
      child.instance_eval(&block) if block_given?
    end
    alias_method :cell, :find_child_and_eval

    def inline_attributes
      return if attributes.empty?

      ' ' + attributes.map { |name, value| Attribute.new(name, value).inline }.join(' ')
    end
  end
end
