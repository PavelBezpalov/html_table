module HtmlTable
  class Table
    attr_reader :rows, :columns, :element, :row_elements
    def initialize(rows:, columns:)
      @rows = rows
      @columns = columns
      @element = Element.new('table')
      @row_elements = []
      @header_row = nil

      create_rows_and_cells
    end

    def to_html
      element.to_html
    end

    def width(value)
      element.attributes[:style] ||= {}
      element.attributes[:style][:width] = value
    end

    def height(value)
      element.attributes[:style] ||= {}
      element.attributes[:style][:height] = value
    end

    def border(value)
      element.attributes[:style] ||= {}
      element.attributes[:style][:border] = value
    end

    def cellpadding(value)
      row_elements.each do |row|
        add_cellpadding(row.children, value)
      end
      add_cellpadding(header_row.children, value) if header_row
    end

    def header(human_index, &block)
      create_header unless header_row

      header_cell = header_row.children[human_index - 1]
      header_cell.instance_eval(&block) if block_given?
    end

    def row(human_index, &block)
      row = row_elements[human_index - 1]
      row.instance_eval(&block) if block_given?
    end

    def classes(value)
      element.classes(value)
    end

    private

    attr_reader :header_row

    def create_header
      @header_row = create_row_with_cells(header: true)
      element.children.prepend(header_row)
    end

    def add_cellpadding(cells, value)
      cells.each do |cell|
        cell.attributes[:style] ||= {}
        cell.attributes[:style][:padding] = value
      end
    end

    def create_rows_and_cells
      @row_elements = Array.new(rows, create_row_with_cells)
      element.children = row_elements.dup
    end

    def create_row_with_cells(header: false)
      row = Element.new('tr')
      cell_tag = header ? 'th' : 'td'

      row.children = Array.new(columns, Element.new(cell_tag))
      row
    end
  end
end