
require_relative 'html_table/attribute'
require_relative 'html_table/element'
require_relative 'html_table/table'
module HtmlTable
  def self.create(rows:, columns:, &block)
    table = Table.new(rows: rows, columns: columns)

    table.instance_eval(&block) if block_given?

    table.to_html
  end
end
